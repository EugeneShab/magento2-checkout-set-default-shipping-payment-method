<?php

namespace ITS\DefaultShippingPaymentMethod\Model;

class ConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    const XPATH_DEFAULT_SHIPPING_METHOD = 'its_default_shipping_payment_method/shipping_method/method';
    const XPATH_DEFAULT_SHIPPING_METHOD_IS_FIRST_OTHERWISE = 'its_default_shipping_payment_method/shipping_method/first_is_default_otherwise';

    const XPATH_DEFAULT_PAYMENT_METHOD = 'its_default_shipping_payment_method/payment_method/method';
    const XPATH_DEFAULT_PAYMENT_METHOD_IS_FIRST_OTHERWISE = 'its_default_shipping_payment_method/payment_method/first_is_default_otherwise';

    /** @var \Magento\Store\Api\Data\StoreInterface */
    protected $store;

    /**
     * ConfigProvider constructor.
     *
     * @param \Magento\Store\Api\Data\StoreInterface $store
     */
    public function __construct(
        \Magento\Store\Api\Data\StoreInterface $store

    ) {
        $this->store = $store;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config = [
            'defaultShippingMethod' => $this->store->getConfig(static::XPATH_DEFAULT_SHIPPING_METHOD),
            'defaultShippingMethodIsFirstOtherwise' => $this->store->getConfig(static::XPATH_DEFAULT_SHIPPING_METHOD_IS_FIRST_OTHERWISE),
            'defaultPaymentMethod' => $this->store->getConfig(static::XPATH_DEFAULT_PAYMENT_METHOD),
            'defaultPaymentMethodIsFirstOtherwise' => $this->store->getConfig(static::XPATH_DEFAULT_PAYMENT_METHOD_IS_FIRST_OTHERWISE)
        ];

        return $config;
    }
}