# Default Shipping Payment Method
The current module allows you to specify which shipping and payment method to choose by default.

## What you need to do to make everything work
Select the required settings in the system config (Intechsoft Extensions tab).

## Troubleshotting
- Do not forget to run the command 'php bin/magento setup:upgrade'
