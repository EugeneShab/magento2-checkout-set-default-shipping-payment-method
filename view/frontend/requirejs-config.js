var config = {
    map: {
        '*': {
            'Magento_Checkout/js/model/shipping-rate-processor/customer-address':'ITS_DefaultShippingPaymentMethod/js/model/shipping-rate-processor/customer-address',
            'Magento_Checkout/js/model/shipping-rate-processor/new-address':'ITS_DefaultShippingPaymentMethod/js/model/shipping-rate-processor/new-address'
        }
    }
};
