define(
    [
        'underscore',
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/payment-service',
        'Magento_Checkout/js/action/select-payment-method'
    ],
    function (
        _,
        $,
        quote,
        checkoutData,
        paymentService,
        selectPaymentMethodAction
    ) {
        'use strict';

        return (function () {
            /**
             * Select method with code
             *
             * @param code
             */
            function selectMethod(code) {
                if (quote.paymentMethod()) {
                    return;
                }

                selectPaymentMethodAction({
                    method: code
                });
                checkoutData.setSelectedPaymentMethod(code);
            }

            /**
             * Getter for default method code
             *
             * @returns {Boolean}
             */
            function getDefaultMethodCode() {
                return window.checkoutConfig.defaultPaymentMethod;
            }

            /**
             * Getter for isSelectFirstOtherwise option if it's true and any method is not selected - first will be selected
             *
             * @returns {Boolean}
             */
            function isSelectFirstOtherwise() {
                return window.checkoutConfig.defaultPaymentMethodIsFirstOtherwise;
            }

            /**
             * Get all available payment methods
             *
             * @returns {Array|*|{*}}
             */
            function getAvailablePaymentMethods() {
                return _.map(paymentService.getAvailablePaymentMethods(), function (payment) {
                    return payment.method;
                });
            }

            /**
             * Select default method
             */
            function selectDefaultMethod() {
                var methodCode = getDefaultMethodCode();

                if (methodCode && getAvailablePaymentMethods().indexOf(methodCode) !== -1) {
                    selectMethod(methodCode);
                } else if (isSelectFirstOtherwise()) {
                    selectMethod(getAvailablePaymentMethods()[0]);
                }
            }

            return selectDefaultMethod;
        })();
    }
);