define(
    [
        'underscore',
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/shipping-service',
        'Magento_Checkout/js/model/shipping-save-processor/default',
        'Magento_Ui/js/model/messageList'
    ],
    function (
        _,
        $,
        quote,
        checkoutData,
        shippingService,
        shippingSaveProcessor,
        messageList
    ) {
        'use strict';

        return (function () {
            /**
             * Select method with code
             *
             * @param code
             */
            function selectMethod(code) {
                if (!checkoutData.getSelectedShippingRate()) {
                    checkoutData.setSelectedShippingRate(code);

                    $('#co-shipping-method-form').submit();
                }

                if (quote.shippingMethod()) {
                    shippingSaveProcessor.saveShippingInformation().fail(function() {
                        messageList.clear();
                    });
                }
            }

            /**
             * Getter for default method code
             *
             * @returns {Boolean}
             */
            function getDefaultMethodCode() {
                return window.checkoutConfig.defaultShippingMethod;
            }

            /**
             * Getter for isSelectFirstOtherwise option if it's true and any method is not selected - first will be selected
             *
             * @returns {Boolean}
             */
            function isSelectFirstOtherwise() {
                return window.checkoutConfig.defaultShippingMethodIsFirstOtherwise;
            }

            /**
             * Get all available shipping methods
             *
             * @returns {Array|*|{*}}
             */
            function getAvailableShippingMethods() {
                return _.map(shippingService.getShippingRates()(), function (rate) {
                    return rate.carrier_code + '_' + rate.method_code;
                });
            }

            /**
             * Select default method
             */
            function selectDefaultMethod() {
                var methodCode = getDefaultMethodCode();

                if (methodCode && getAvailableShippingMethods().indexOf(methodCode) !== -1) {
                    selectMethod(methodCode);
                } else if (isSelectFirstOtherwise()) {
                    selectMethod(getAvailableShippingMethods()[0]);
                }
            }

            return selectDefaultMethod;
        })();
    }
);